# -*- coding: utf-8 -*-
"""
Úkol 6.
Vaším dnešním úkolem je vytvořit program, který o zadaném textu zjistí některé
údaje a vypíše je na standardní výstup. Hlavním smyslem cvičení je procvičit
si práci s regulárními výrazy, takže pro plný bodový zisk je nutné použít k
řešení právě tento nástroj.

Program musí pracovat s obecným textem, který bude zadaný v souboru. Jméno
souboru bude zadáno jako vstupní parametr funkce main, která by měla být
vstupním bodem programu. Samozřejmě, že funkce main by neměla řešit problém
kompletně a měli byste si vytvořit další pomocné funkce. Můžete předpokládat,
že soubor bude mít vždy kódování utf-8 a že bude psaný anglicky, tedy jen
pomocí ASCII písmen, bez české (či jiné) diakritiky.

Konkrétně musí program zjistit a vypsat:

1. Počet slov, která obsahují právě dvě samohlásky (aeiou) za sebou. Například
slovo bear.

2. Počet slov, která obsahují alespoň tři samohlásky - například slovo atomic.

3. Počet slov, která mají šest a více znaků - například slovo terrible.

4. Počet řádků, které obsahují nějaké slovo dvakrát.

Podrobnější zadání včetně příkladu je jako obvykle na elearning.tul.cz
"""
import re

def main(file_name):
    """
    zpracujte soubor
    """
    file_by_line = read_file(file_name)
    fourth_task = analyze_lines(file_by_line, re.compile(r'\b(\w+)\b(?=.*?\b\1\b)', re.IGNORECASE))
    words_on_lines = do_split(file_by_line)

    print(analyze_words(words_on_lines, re.compile(r'\w*[aeiyou]{2,}\w*')))
    print(analyze_words(words_on_lines, re.compile(r'((\w*[aeiyou]\w*){3,})')))
    print(analyze_words(words_on_lines, re.compile(r'\w{6,}')))
    print(fourth_task)

def read_file(file_name):
    """
    načte soubor
    """
    input_file = open(file_name, "r")
    lines = input_file.readlines()
    input_file.close()
    return lines

def do_split(lines):
    """
    rozdělí na slova
    """
    j = 0
    for i in lines:
        lines[j] = i.lower().replace("\n", "").split(" ")
        j += 1
    return lines

def analyze_words(words_on_lines, pattern):
    """
    analyzuje dle paternu - slova
    """
    lis = []
    for i in words_on_lines:
        for j in i:
            if re.search(pattern, j):
                if j not in lis:
                    lis.append(j)
    return len(lis)

def analyze_lines(file_by_line, pattern):
    """
    analyzuje dle paternu - celé řádky
    """
    count = 0
    for i in file_by_line:
        print(i)
        if re.search(pattern, i):
            count += 1
    return count

if __name__ == '__main__':
    main('simple.txt')
