# -*- coding: utf-8 -*-
"""
@TODO implementujte dle zadĂˇnĂ­ cviÄŤenĂ­ 8
"""
class Card:
    """
    TĹ™Ă­da pro reprezentaci hracĂ­ch karet
    """
    def __init__(self, given_rank, given_suit):
        self._rank = None
        self._suit = None
        self.rank = given_rank
        self.suit = given_suit

    def _set_rank(self, val):
        """set rank"""
        if val < 2 or val > 14:
            raise TypeError("Wrong value")
        self._rank = val

    def _set_suit(self, suit):
        """set suit"""
        if suit in ('s', 'k', 'p', 't'):
            self._suit = suit
        else:
            raise TypeError("Wrong type")
    def _get_rank(self):
        """get rank"""
        return self._rank
    def _get_suit(self):
        """get suit"""
        return self._suit

    rank = property(_get_rank, _set_rank)
    suit = property(_get_suit, _set_suit)

    def black_jack_rank(self):
        """
        Metoda vracĂ­ hodnotu karty dle pravidel pro Black Jack
        :return:
        """
        if self._rank < 11:
            return self._rank
        if self._rank == 14:
            return 11
        return 10



    def __str__(self):
        """to string method"""
        values = ["dvojka", "trojka", "čtyřka", "pětka", "šestka", "sedma", "osma", "devítka",
    "desítka", "spodek", "královna", "král", "eso"]
        if self._rank < 11 or self._rank == 12:
            if self._suit == "s":
                return "srdcová " + values[self._rank-2]
            if self._suit == "k":
                return "kárová " + values[self._rank-2]
            if self._suit == "p":
                return "piková " + values[self._rank-2]
            if self._suit == "t":
                return "trefová " + values[self._rank-2]
        if self._rank == 11 or self._rank == 13:
            if self._suit == "s":
                return "srdcový " + values[self._rank-2]
            if self._suit == "k":
                return "kárový " + values[self._rank-2]
            if self._suit == "p":
                return "pikový " + values[self._rank-2]
            if self._suit == "t":
                return "trefový " + values[self._rank-2]
        if self._rank == 14:
            if self._suit == "s":
                return "srdcové " + values[self._rank-2]
            if self._suit == "k":
                return "kárové " + values[self._rank-2]
            if self._suit == "p":
                return "pikové " + values[self._rank-2]
            if self._suit == "t":
                return "trefové " + values[self._rank-2]
        raise Exception("should´t happend")

    def __eq__(self, other):
        """is equal"""
        return self.black_jack_rank() == other.black_jack_rank()
    def __gt__(self, other):
        """is greter"""
        return self.black_jack_rank() > other.black_jack_rank()
    def __lt__(self, other):
        """is smaller"""
        return self.black_jack_rank() < other.black_jack_rank()
    def __ge__(self, other):
        """is equal or gretter"""
        return self.black_jack_rank() >= other.black_jack_rank()
    def __le__(self, other):
        """is equal or smaller"""
        return self.black_jack_rank() <= other.black_jack_rank()
