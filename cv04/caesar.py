# -*- coding: utf-8 -*-
"""
Vytvorte funkce encrypt a decrypt pro Caesarovu sifru.
Kompletni zadani v elearningu.
"""
def encrypt(word, offset):
    """
    :param word - slovo k zasifrovani
    :param offset - znakovy posun
    :return: zasifrovane slovo
    """
    result = ""
    for i in range(len(word)):
        result += move_char(word[i], offset)
    return result


def decrypt(word, offset):
    """
    :param word - zasifrovane slovo
    :param offset - znakovy posun
    :return: desifrovane slovo
    """
    result = ""
    for i in range(len(word)):
        result += move_char(word[i], -offset)
    return result


def move_char(char, offset):
    """
    :param char - znak, ktery ma byt posunut
    :param offset - o kolik ma byt posunuto
    :return char - posunuty znak
    """
    charord = ord(char)
    if 172 > charord < 65 or 141 < charord > 132:
        return char
    if char.isupper():
        asci = charord + offset - 65
        return chr(asci % 26 + 65)

    asci = charord + offset - 97
    return chr(asci % 26 + 97)
