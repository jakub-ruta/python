#!/bin/env python
# -*- coding: utf-8 -*-
import math
"""
PJP - cvičení číslo 2
"""
def is_convex(a, b, c, d):
    """
    Druhým úkolem je vytvořit funkci, která ze čtyř zadaných bodů určí,
    zda tvoří konvexní čtyřúhelník.

    Body na vstupu jsou zadávány jako tuple (x, y) kde x a y mohou být
    libovolná reálná čísla, tedy i záporná. Body mohou vytvořit čtyřúhelník,
    ale není to pravidlem.

    Je potřeba aby funkce hlídala i extrémní situace, jako například,
    že body čtyřúhelník vůbec nevytváří.
    """
    ABC_c = return_lenght(a, b);
    ABC_a = return_lenght(b, c);
    ABC_b = return_lenght(c, a);
    beta = return_angle(ABC_a, ABC_b, ABC_c);
    if beta == 180 or beta == False:
        return False;
    BCD_c = return_lenght(b, c);
    BCD_a = return_lenght(c, d);
    BCD_b = return_lenght(d, b);
    gama = return_angle(BCD_a, BCD_b, BCD_c);
    if gama == 180 or gama == False:
        return False;
    CDA_c = return_lenght(c, d);
    CDA_a = return_lenght(d, a);
    CDA_b = return_lenght(a, c);
    delta = return_angle(CDA_a, CDA_b, CDA_c);
    if delta == 180 or delta == False:
        return False;
    DAB_c = return_lenght(d, a);
    DAB_a = return_lenght(a, b);
    DAB_b = return_lenght(b, d);
    alfa = return_angle(DAB_a, DAB_b, DAB_c);
    if alfa == 180 or alfa == False:
        return False;
    soucet = alfa + beta + gama + delta;
    print(soucet);
    if int(soucet) < 360:
        print("stop1")
        return False
    if int(soucet) > 360:
        print("stop2")
        return False
    return True




def return_angle(a, b, c):
    if (a == 0 or b == 0 or c == 0):
        return False
    beta = (((c * c) + (a * a)-(b * b)) / (2 * c * a));
    radian = math.acos(beta);
    return (radian  * 180 / math.pi);
def return_lenght(a, b):
    return abs(math.sqrt((b[0]-a[0]) ** 2 + (b[1] - a[1]) ** 2));


if __name__ == '__main__':
    is_convex((0.0, 0.0), (1.0, 0.0), (1.0, 1.0), (0.0, 0.0))
