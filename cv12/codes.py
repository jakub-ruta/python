# -*- coding: utf-8 -*-

"""
@TODO - vyřešit úkol 12.

Podrobné zadání jako obvykle na https://elearning.tul.cz

"""
from itertools import combinations
import time

def store_in_file(text, file_name):
    """
    Uloží list po řádcích do souboru

    """
    text_formated = ''
    for number in text:
        text_formated += str(number) + "\n"
    with open(file_name, 'w') as output:
        output.write(text_formated)

def primes(max_number):
    """ Returns  a list of primes < n
    from:
    https://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n/3035188#3035188
    """
    sieve = [True] * max_number
    for i in range(3, int(max_number ** 0.5) + 1, 2):
        if sieve[i]:
            sieve[i * i::2 * i] = [False] * ((max_number-i * i-1) // (2 * i) + 1)
    return [2] + [i for i in range(3, max_number, 2) if sieve[i]]

def filter_primes(list_of_primes):
    """
    vyfiltruje prvnočísla, které zadání vylučuje
    """
    new_list = []
    for prime in list_of_primes:
        if prime > 99999:
            string = str(prime)
            if string.count("1") > 2:
                new_list.append(prime)
                continue
            if string.count("2") > 2:
                new_list.append(prime)
                continue
            if string.count("3") > 2:
                new_list.append(prime)
                continue
            if string.count("4") > 2:
                new_list.append(prime)
                continue
            if string.count("5") > 2:
                new_list.append(prime)
                continue
            if string.count("6") > 2:
                new_list.append(prime)
                continue
            if string.count("7") > 2:
                new_list.append(prime)
                continue
            if string.count("8") > 2:
                new_list.append(prime)
                continue
            if string.count("9") > 2:
                new_list.append(prime)
                continue
    return new_list

def do_magic(primes_numbers, tuples_change):
    """
    najde kód nebo vyhodí vyjímku
    """
    for tuple_val in tuples_change:
        for i in range(100, 1000):
            cond = 0
            number = str(i)
            new_code = []
            for j in range(10):
                pos = 0
                new_number = ''
                for k in range(6):
                    if k in tuple_val:
                        new_number += str(j)
                    else:
                        new_number += number[pos]
                        pos += 1
                if int(new_number) in primes_numbers:
                    cond += 1
                    new_code.append(new_number)
                if cond == 8:
                    return new_code
    raise Exception("Not found")

if __name__ == '__main__':
    start_time = time.time()
    primes = filter_primes(primes(999999))
    change_able = list(combinations(range(6), 3))
    founded = do_magic(primes, change_able)
    store_in_file(founded, "new_codes.txt")
    #print(founded)
    #print("--- %s seconds ---" % (time.time() - start_time))
