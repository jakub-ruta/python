# -*- coding: utf-8 -*-

"""
@TODO: zde napiste svoje unit testy pro program codes.py
"""
import codes
from itertools import combinations
import pytest

def test_primes():
    result = [2, 3, 5, 7]
    assert codes.primes(10) == result

def test_filter_primes1():
    test = [100, 100000, 111000, 112233, 122234, 54679888, 1111, 555]
    result = [111000, 122234, 54679888]
    assert codes.filter_primes(test) == result

def test_filter_primes2():
    test = codes.primes(100500)
    result = [100151, 100333, 100411]
    assert codes.filter_primes(test) == result

def test_do_magic():
    """
    Na takto moc konkrétní metodu nelze napsat obecný test :/ Stejně nevím jak
    bych to napsal do více metod
    """

    result = ['121313', '222323', '323333', '424343', '525353', '626363', '828383', '929393']
    assert codes.do_magic(codes.filter_primes(codes.primes(999999)),
                          list(combinations(range(6), 3))) == result

