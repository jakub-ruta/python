# -*- coding: utf-8 -*-

"""
Úkol 5.
Napište program, který načte soubor large.txt a pro každé dveře vyhodnotí,
zda je možné je otevřít nebo ne. Tedy vyhodnotí, zda lze danou množinu uspořádat
požadovaným způsobem. Výstup z programu uložte do souboru vysledky.txt ve
formátu 1 výsledek =  1 řádek. Na řádek napište vždy počet slov v množině a True
nebo False, podle toho, zda řešení existuje nebo neexistuje.

Podrobnější zadání včetně příkladu je jako obvykle na elearning.tul.cz
"""
if __name__ == '__main__':
    inputFile = open("large.txt", "r")
    outputFile = open("vysledky.txt", "w")
    fLetters = {}
    lLetters = {}
    for i in range(int(inputFile.readline())):
        count = int(inputFile.readline())
        for j in range(26):
            fLetters[j] = 0
            lLetters[j] = 0
        for j in range(int(count)):
            string = inputFile.readline()
            fLetters[ord(string[0]) - 97] += 1
            lLetters[ord(string[-2]) - 97] += 1

        print(fLetters)
        print(lLetters)
        countWrong = 0
        for j in range(26):
            countE = fLetters[j]-lLetters[j]
            if countE > 1 or countE < -1:
                countWrong += 2
            elif countE == 1 or countE == -1:
                countWrong += 1

        if countWrong in (0, 2):
            outputFile.write(str(count) + " True\n")
        else:
            outputFile.write(str(count) + " False\n")
