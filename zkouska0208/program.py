# -*- coding: utf-8 -*-
import argparse
from bs4 import BeautifulSoup as bs
import time


DEFLECTION = 0.002
"""
@TODO - vyřešit úkol zkouska 08022021

Podrobné zadání jako obvykle na https://elearning.tul.cz

"""
def parse_args():
    """
   get arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--track", required=True, help="GPS data")
    parser.add_argument("-p", "--points", required=True, help="Tested points")

    args = parser.parse_args()

    return args.track, args.points

def read_file(file_name):
    """
    načte soubor
    """
    input_file = open(file_name, "r")
    lines = input_file.read()
    input_file.close()
    return lines.strip()

def read_file_by_lanes(file_name):
    """
    načte soubor
    """
    input_file = open(file_name, "r")
    lines = input_file.readlines()
    input_file.close()
    return lines

def parse_track(var_string):
    """
    make list from lines - use BS xml parser - return list of tuples with lat and len
    """
    data_list = []
    track_points = bs(var_string, "xml").find_all("trkpt")
    for track_point in track_points:
        data_list.append(tuple((track_point['lat'], track_point['lon'])))
    return data_list

def parse_points(var_lines):
    """
    parse points from file and return dict of points
    """
    data_list = {}
    for var_line in var_lines:
        line = var_line.rsplit(" ", 3)

        data_list[line[0]] = tuple((line[1], line[3].replace("\n", "")))
    return data_list

def find_matches(var_tested_points, var_track_data):
    """
    find matches
    """
    points = var_tested_points
    not_founded = len(points)-1
    founded = {}
    for point in points:
        founded[point] = False
    for track in var_track_data:
        for foun in founded:
            if not founded[foun]:
                if is_go_throw(float(track[0]), float(track[1]), float(points[foun][0]), float(points[foun][1])):
                    founded[foun] = True
                    not_founded = -1
            if not_founded == 0:
                return founded
    return founded




def is_go_throw(track_lat, track_len, point_lat, point_len):
    """
    simple method useing deflection to tell if point in on track
    """
    if (point_lat - DEFLECTION) < track_lat < (point_lat + DEFLECTION) and (point_len - DEFLECTION) < track_len < (point_len + DEFLECTION):
        return True
    return False

#def make_envelope(var_tested_points):
#    for point in var_tested_points:
#        var_tested_points[point] = tuple((tuple((float(var_tested_points[point][0])-DEFLECTION, float(var_tested_points[point][0]) + DEFLECTION)), tuple((float(var_tested_points[point][1])-DEFLECTION, float(var_tested_points[point][1]) + DEFLECTION))))
#    return var_tested_points

def print_result(results):
    """
    for print result on console
    """
    for res in results:
        print(res + " " + str(results[res]))


if __name__ == '__main__':
    start_time = time.time()
    track, points = parse_args()
    tested_points = parse_points(read_file_by_lanes(points))
    #print(tested_points)
    track_data = parse_track(read_file(track))
    #print(track_data)
    result = find_matches(tested_points, track_data)
    print_result(result)
    #print("--- %s seconds ---" % (time.time() - start_time))
