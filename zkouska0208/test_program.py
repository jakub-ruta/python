# -*- coding: utf-8 -*-

"""
@TODO: zde napiste svoje unit testy pro program program.py
"""
import program
import pytest

def test_parse_track():
    test = '<trkpt lat="39.5098060" lon="2.7509090">    <ele>24.6</ele>    <time>2015-03-12T09:01:28Z</time>    <extensions>     <gpxtpx:TrackPointExtension>      <gpxtpx:hr>86</gpxtpx:hr>      <gpxtpx:cad>0</gpxtpx:cad>     </gpxtpx:TrackPointExtension>    </extensions>   </trkpt>'
    result = [('39.5098060', '2.7509090')]
    assert program.parse_track(test) == result

def test_parse_points():
    test = ['Puig Mayor 39.795018 / 2.790312\n', 'Monte Caro 40.80374 / 0.34393']

    result = {'Monte Caro': ('40.80374', '0.34393'), 'Puig Mayor': ('39.795018', '2.790312')}
    assert program.parse_points(test) == result

def test_find_matches():
    test_points = {'Monte Caro': ('40.80374', '0.34393'), 'Puig Mayor': ('39.795018', '2.790312')}
    track_data = [('39.5098060', '2.7509090'), ('39.795018', '2.790312')]
    result = {'Monte Caro': False, 'Puig Mayor': True}
    assert program.find_matches(test_points, track_data) == result

def test_is_go_throw():
    track_lat = 39.795018
    track_len = 2.790312
    point_lat = 39.795018
    point_len = 2.790312

    result = True
    assert program.is_go_throw(track_lat, track_len, point_lat, point_len) == result



