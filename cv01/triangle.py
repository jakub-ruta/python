# -*- coding: utf8 -*-
"""
Zakladni sablona pro prvni cviceni
"""
def triangle(a, b, c):
    """
    Funkce vrací True nebo False, podle toho zda strany a, b, c mohou tvořit
    pravoúhlý trojúhelník

    Pro jednoduchost můžete předpokládat, že strany a, b jsou odvěsny, c je přepona.
    Tak jako je to ve známé matematické poučce.
    """
    d = a * a + b * b;
    c2 = c ** 2;
    if (d == c2):return True;

    return False

