# -*- coding: utf-8 -*-

"""
@TODO: zde napiste svoje unit testy pro modul censor.py
"""
import censor
import pytest
import sys

def test_replace():
    words = ['ipsum', 'amet', 'arcu', 'ipsam']
    text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque arcu. Nemo enim ipsam voluptatem quia"
    result = "Lorem ##### dolor sit ####, consectetuer adipiscing elit. Pellentesque ####. Nemo enim ##### voluptatem quia"
    assert censor.replace(text, words) == result

def test_clean_tags():
    text = "<p>Lorem<div> ipsum dolor sit amet, consectetuer <b>adipiscing elit. Pellentesque arcu. <strong>Nemo</p> enim ipsam voluptatem quia"
    result = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque arcu. Nemo enim ipsam voluptatem quia"
    assert censor.clean_tags(text) == result

