# -*- coding: utf-8 -*-
import argparse
import re
"""
@TODO - vyřešit úkol 11. - filtrování textu

Podrobné zadání jako obvykle na https://elearning.tul.cz

"""
def parse_args():
    """
   get arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", required=True, help="HTML file.")
    parser.add_argument("-l", "--list", required=True, help="blocked words")
    parser.add_argument("-c", "--clean", nargs='?', const=True, default=False, help="remove tags")
    parser.add_argument("-o", "--output", nargs='?',
                        default=None, help="save to file. if not set print to final to console")

    args = parser.parse_args()

    return args.input, args.list, args.clean, args.output

def read_file(file_name):
    """
    načte soubor
    """
    input_file = open(file_name, "r")
    lines = input_file.read()
    input_file.close()
    return lines.strip()

def read_file_by_lanes(file_name):
    """
    načte soubor
    """
    input_file = open(file_name, "r")
    lines = input_file.readlines()
    input_file.close()
    return lines

def store_file(text, file_name):
    """
    Uloží list slovníků do souboru output.json tak jak je požadováno
    v zadání.
    """
    with open(file_name, 'w') as output:
        output.write(text)

def replace(data_string, words_list):
    """
    nahradí slova v textu
    """
    for word in words_list:
        replace_string = ''
        word = word.strip()
        print(word)
        for i in range(len(word)):
            replace_string = replace_string + "#"
        data_string = data_string.replace(word, replace_string)
    return data_string.strip()

def clean_tags(replaced):
    """
    vymaže html tagy
    """
    tag_pattern = re.compile(r'<.*?>')
    return tag_pattern.sub('', replaced).strip()

if __name__ == '__main__':
    input_file_path, blocked, tags, output_file = parse_args()
    data = read_file(input_file_path)
    words = read_file_by_lanes(blocked)
    replaced_string = replace(data, words)
    if tags:
        final = clean_tags(replaced_string)
    else:
        final = replaced_string
    if output_file:
        store_file(final, output_file)
    else:
        print(final)
