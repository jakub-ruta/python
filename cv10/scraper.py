# -*- coding: utf-8 -*-


from bs4 import BeautifulSoup
import json
import re
import requests
import sys
dict_pages = {}
email_pattern = re.compile("([a-zA-Z0-9+.!#$%&'*+/=?^_`{|}~-]+(?:@|#)[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+)")

def get_all_urls(entry_url, pages):
    """Rekurzivní metoda pro nalezené všech odkazů"""
    file = requests.get(entry_url)
    if entry_url.endswith(".html"):
        pos = entry_url.rfind("/")
        actual = entry_url[pos + 1:]
    else:
        actual = "index.html"
    pages.append(actual)
    soup = BeautifulSoup(file.text, "html.parser")
    hrefs = soup.findAll("a", attrs={"href":True})
    dict_pages[actual] = []
    for href in hrefs:
        if href["href"] in pages:
            break
        next_page = entry_url.replace(actual, "") + "/" + href["href"]
        next_url = requests.get(next_page)
        if next_url.status_code == 200:
            pages.append(href["href"])
            dict_pages[actual].append(href["href"])
            pages = list(set(pages) | set(get_all_urls(next_page, pages)))
    return pages

def find_emails_in_pages(base_url, pages):
    """získání stránek pro maily"""
    mails = []
    entry_url = ""
    if base_url.endswith(".html"):
        pos = entry_url.rfind("/")
        base_url = entry_url[:pos]
    for page in pages:
        f = requests.get(base_url + page)
        mails = find_mails(f.text, mails)
    return mails

def find_mails(text, already_foounded_mails):
    """nalezení mailů"""
    emails = re.findall(email_pattern, text)
    already_foounded_mails = list(set(already_foounded_mails) | set(emails))
    return already_foounded_mails

def write_to_file(pages, emails):
    """zápis do souboru"""
    f = open("scrap_result.txt", "w")
    f.write(json.dumps(pages, indent=4, sort_keys=True))
    f.write('\n\n')
    for mail in emails:
        f.write(mail)
        f.write("\n")
    f.close()


if __name__ == '__main__':
    pages = []
    pages = get_all_urls(sys.argv[1], pages)
    print(dict_pages)
    email = find_emails_in_pages(sys.argv[1], pages)
    print(email)
    write_to_file(pages, email)
